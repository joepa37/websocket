<?php

namespace app\modules\websocket\widgets;

use app\modules\websocket\assets\WebSocketAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\web\View;

class Printer extends Widget
{
	/**
	 * Enable/Disable JavaScript Debugging
	 * @var boolean
	 */
	public $debug = false;

	/**
	 * Html to render to echo on the view taking the template to determines how to organize the sections.
	 * @var string
	 */
	public $layout = "";

	/**
	 * Html template to render the entire widget
	 * @var string
	 */
	public $template = <<<HTML
{alerts}\n
<div class="col-md-3">
    <div id="qz-connection" class="panel panel-default">
	    <div class="panel-heading">
	        {launchButton}\n
	        {printButton}\n
	        {statusHeading}\n
	    </div>
	</div>
</div>
HTML;

	/**
	 * Html to render the alerts
	 * @var string
	 */
	public $alerts = <<<HTML
<div id="qz-alert" style="position: fixed; width: 30%; margin: -65px 4% 0 0; z-index: 900;"></div>
HTML;

	/**
	 * Html to render the button to launch QZ Tray Software
	 * @var string
	 */
	public $launchButton = <<<HTML
<button class="close tip" data-toggle="tooltip" title="Launch QZ" id="launch" href="#" onclick="launchQZ();" style="display: none;">
    <i class="fa fa-external-link"></i>
</button>
HTML;

	/**
	 * Html to render the button to print when the [printOnInit] is false
	 * @var string
	 */
	public $printButton = <<<HTML
<button class="close tip" data-toggle="tooltip" title="Imprimir" id="print" href="#" onclick="printData();" disabled>
    <i class="fa fa-print"></i>
</button>
HTML;

	/**
	 * Html to render the heading
	 * @var string
	 */
	public $statusHeading = <<<HTML
<h3 class="panel-title">
    Estado: <span id="qz-status" class="text-muted" style="font-weight: bold;">Esperando</span>
</h3>
HTML;

	/**
	 * Printer default configuration
	 * @var array
	 *
	 * {string} [colorType='color'] Valid values [color | grayscale | blackwhite]
	 * {number} [copies=1] Number of copies to be printed.
	 * {number|Array<number>} [density=72] Pixel density (DPI, DPMM, or DPCM depending on [units]).
	 *     If provided as an array, uses the first supported density found (or the first entry if none found).
	 * {boolean} [duplex=false] Double sided printing
	 * {number} [fallbackDensity=null] Value used when default density value cannot be read, or in cases where reported as "Normal" by the driver, (in DPI, DPMM, or DPCM depending on [units]).
	 * {string} [interpolation='bicubic'] Valid values [bicubic | bilinear | nearest-neighbor]. Controls how images are handled when resized.
	 * {string} [jobName=null] Name to display in print queue.
	 * {Object|number} [margins=0] If just a number is provided, it is used as the margin for all sides.
	 * {number} [margins.top=0]
	 * {number} [margins.right=0]
	 * {number} [margins.bottom=0]
	 * {number} [margins.left=0]
	 * {string} [orientation=null] Valid values [portrait | landscape | reverse-landscape]
	 * {number} [paperThickness=null]
	 * {string} [printerTray=null]
	 * {boolean} [rasterize=true] Whether documents should be rasterized before printing. Forced TRUE if [density] is specified.
	 * {number} [rotation=0] Image rotation in degrees.
	 * {boolean} [scaleContent=true] Scales print content to page size, keeping ratio.
	 * {Object} [size=null] Paper size.
	 * {number} [size.width=null] Page width.
	 * {number} [size.height=null] Page height.
	 * {string} [units='in'] Page units, applies to paper size, margins, and density. Valid value [in | cm | mm]
	 *
	 * {boolean} [altPrinting=false] Print the specified file using CUPS command line arguments.  Has no effect on Windows.
	 * {string} [encoding=null] Character set
	 * {string} [endOfDoc=null]
	 * {number} [perSpool=1] Number of pages per spool.
	 */
	public $config = [
		'colorType' => 'color',
		'copies' => 1,
		'density' => 0,
		'duplex' => false,
		'fallbackDensity' => null,
		'interpolation' => 'bicubic',
		'jobName' => null,
		'margins' => 0,
		'orientation' => null,
		'paperThickness' => null,
		'printerTray' => null,
		'rasterize' => true,
		'rotation' => 0,
		'scaleContent' => true,
		'size' => null,
		'units' => 'in',
		'altPrinting' => false,
		'encoding' => null,
		'endOfDoc' => null,
		'perSpool' => 1,
	];

	/**
	 * Use the default printer. If is true, don't need to specify the [printer]
	 * @var boolean
	 */
	public $useDefaultPrinter = true;

	/**
	 * The printer to be used. If is set override the [useDefaultPrinter]
	 * @var boolean
	 */
	public $printer;

	/**
	 * @var array
	 * Array of data being sent to the printer.
	 * String values are interpreted the same as the default [raw] object value.
	 * Print requests will be pre-signed.
	 *
	 * {string} [type] Valid values [html | image | pdf | raw]
	 * {string} [format] Format of data provided.
	 *      For [html] types, valid formats include [file(default) | plain]
	 *      For [image] types, valid formats include [base64 | file(default)]
	 *      For [pdf] types, valid format include [base64 | file(default)]
	 *      For [raw] types, valid formats include [base64 | file | hex | plain(default) | image | xml]
	 * {string} [data] The information to be passed or converted depends of the format
	 * {string} [options.language] Required with [raw] type [image] format. Printer language.
	 * {number} [options.x] Optional with [raw] type [image] format. The X position of the image.
	 * {number} [options.y] Optional with [raw] type [image] format. The Y position of the image.
	 * {string|number} [options.dotDensity] Optional with [raw] type [image] format.
	 * {string} [options.xmlTag] Required with [xml] format. Tag name containing base64 formatted data.
	 * {number} [options.pageWidth] Optional with [html] type printing. Width of the web page to render. Defaults to paper width.
	 * {number} [options.pageHeight] Optional with [html] type printing. Height of the web page to render. Defaults to adjusted web page height.
	 * {boolean} [signature] Pre-signed signature of JSON string containing call, params, and timestamp.
	 * {number} [signingTimestamp] Required with [signature]. Timestamp used with pre-signed content.
	 */
	public $data = [
		'type' => 'raw',
		'format' => 'raw',
		'data' => null,
		'options' => null,
	];

	/**
	 * Trigger print when the connection is established
	 * @var boolean
	 */
	public $printOnInit = false;

	/**
	 * JS Triggers to be executed before and after an action
	 * @var string
	 */
	public $BEFORE_CONNECT;
	public $AFTER_CONNECT;
	public $BEFORE_SET_PRINTER;
	public $AFTER_SET_PRINTER;
	public $BEFORE_PRINT;
	public $AFTER_PRINT;


	/**
	 * Initializes the object.
	 * This method is called at the end of the constructor.
	 */
	public function run()
	{
		$this->initLayout();
		$this->registerAssets();
		$this->registerScript();
		parent::run();
	}

	/**
	 * Sets the widget layout based on the [[template]] setting.
	 */
	protected function initLayout(){
		$this->layout = strtr(
			$this->template,
			[
				'{alerts}' => $this->alerts,
				'{launchButton}' => $this->launchButton,
				'{printButton}' => $this->printButton,
				'{statusHeading}' => $this->statusHeading,
			]
		);
		if($this->layout){
			echo $this->layout;
		}
	}

	/**
	 * Registers assets
	 */
	protected function registerAssets(){
		WebSocketAsset::register($this->getView());
	}

	/**
	 * Registers script
	 */
	protected function registerScript()
	{
		$debug = json_encode($this->debug);

		if($this->config !== null){
			$config = json_encode($this->config);
		}else{
			throw new InvalidConfigException('The "Printer Configuration" property must be set.');
		}

		//Connection function and printer query setting
		if($this->useDefaultPrinter && $this->printer == null){
			$findPrinter = <<<JS
findDefaultPrinter(true);
JS;
		}else if($this->printer !== null){
			$printer = json_encode($this->printer);
			$findPrinter = <<<JS
findPrinter({$printer}, true);
JS;
		}else{
			throw new InvalidConfigException('The "Printer Name" property must be set if is not using the "Default Printer".');
		}

		//Data to be sent for printing
		if($this->data !== null && isset($this->data[0]) && is_array($this->data[0])){
			if(isset($this->data[0]['data'])){
				$data = json_encode($this->data);
			}else{
				throw new InvalidConfigException('The "data[index(0)][\'data\']" property must be set to send it to the printing trigger.');
			}
		}else{
			throw new InvalidConfigException('The "data" property must be set as a multidimensional array to send it to the printing trigger.');
		}

		//Trigger print after the connection is established
		if($this->printOnInit){
			$printOnInit = <<<JS
printData();
JS;
		}else{
			$printOnInit = null;
		}

		$js = <<<JS
var debug = {$debug};
qz.websocket.setClosedCallbacks(function(evt) {
    updateState('Inactivo', 'default');
    if(debug){ 
        console.log(evt);
    }
    if (evt.reason) {
        displayMessage("<strong>Conexión cerrada:</strong> " + evt.reason, 'alert-warning');
    }
});
qz.websocket.setErrorCallbacks(handleConnectionError);

function startConnection(config) {
    if (!qz.websocket.isActive()) {
        updateState('Esperando', 'default');
        {$this->BEFORE_CONNECT}
        qz.websocket.connect(config).then(function() {
            updateState('Activo', 'success');
        	{$this->AFTER_CONNECT}
            {$findPrinter}
        }).catch(handleConnectionError);
    } else {
        displayMessage('Ya existe una conexión activa de QZ.', 'alert-warning');
    }
}
function updateState(text, css) {
    $("#qz-status").html(text);
    $("#qz-connection").removeClass().addClass('panel panel-' + css);
	if(text === "Inactivo" || text === "Error"){
	     $("#launch").show();
	}
    if (text === "Inactivo" || text === "Error" || text === "Esperando") {
        $("#print").attr('disabled','disabled');
    } else {
        $("#launch").hide();
        $("#print").removeAttr('disabled');
    }
}
function handleConnectionError(err) {
    updateState('Error', 'danger');
    if (err.target != undefined) {
        if (err.target.readyState >= 2) {
            displayError("La conexión a QZ Tray se ha cerrado");
        } else {
            displayError("Ha ocurrido un error, verifique el registro para más detalles");
            if(debug){ 
                console.error(err); 
            }
        }
    } else {
        displayError(err);
    }
}
function displayError(err) {
    if(debug){ 
        console.error(err); 
    }
    displayMessage(err, 'alert-danger');
}
function displayMessage(msg, css) {
    if (css == undefined) { css = 'alert-info'; }

    var timeout = setTimeout(function() { $('#' + timeout).alert('close'); }, 5000);

    var alert = $("<div/>").addClass('alert alert-dismissible fade in ' + css)
        .css('max-height', '20em').css('overflow', 'auto')
        .attr('id', timeout).attr('role', 'alert');
    alert.html("<button type='button' class='close' data-dismiss='alert'>&times;</button>" + msg);

    $("#qz-alert").append(alert);
}
function launchQZ() {
    if (!qz.websocket.isActive()) {
        window.location.assign("qz:launch");
        startConnection({ retries: 5, delay: 1 });
    }
}
var cfg = null;
function getUpdatedConfig() {
    if (cfg == null) {
        cfg = qz.configs.create(null);
    }
	cfg.reconfigure({$config});
    return cfg
}
function findPrinter(query, set) {
    qz.printers.find(query).then(function(data) {
        displayMessage("<strong>Impresora:</strong> " + data);
        if (set) { setPrinter(data); }
    }).catch(displayError);
}
function findDefaultPrinter(set) {
    qz.printers.getDefault().then(function(data) {
        displayMessage("<strong>Impresora:</strong> " + data);
        if (set) { setPrinter(data); }
    }).catch(displayError);
}
function setPrinter(printer) {
    {$this->BEFORE_SET_PRINTER}
    var cf = getUpdatedConfig();
    cf.setPrinter(printer);
    {$this->AFTER_SET_PRINTER}
    {$printOnInit}
}
function printData() {
  	var config = getUpdatedConfig();
	var data = $data;
	{$this->BEFORE_PRINT}
	qz.print(config, data).then(function() {
	    if(debug){ 
	        console.log("Printed successfully");
	    }
        {$this->AFTER_PRINT}
    }).catch(displayError);
}
startConnection();
JS;
		$this->getView()->registerJs($js, View::POS_END);
	}
}