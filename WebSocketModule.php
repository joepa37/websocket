<?php
/**
 * @link https://plus.google.com/+joepa37/
 * @copyright Copyright (c) 2017 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace app\modules\websocket;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * WebSocket Module
 *
 * @author José Peña <joepa37@gmail.com>
 */
class WebSocketModule extends \yii\base\Module
{
    /**
     * Version number of the module.
     */
    const VERSION = '0.1.0';

	/**
	 * @inheritdoc
	 */
    public $controllerNamespace = 'app\modules\websocket\controllers';
}