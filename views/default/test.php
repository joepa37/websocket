<?php

use app\modules\websocket\assets\WebSocketAsset;

/* @var $this yii\web\View */

$this->title = 'Web Socket Test';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
WebSocketAsset::register($this);

$js = <<<JS
qz.websocket.connect().then(function() {
    return qz.printers.find("HP")               // Pass the printer name into the next Promise
}).then(function(printer) {
var config = qz.configs.create(
        printer,                                // Printer returned on the last Promise
        { jobName: "Esta es una prueba" }       // Config to apply to the current print
    );
var data = [{                                   //PDF file
    type: 'pdf', 
    data: webSocketBaseUrl + '/samples/pdf_sample.pdf' 
}];
    return qz.print(config, data);
}).catch(function(e) { console.error(e); });
JS;

$this->registerJs($js);
?>


