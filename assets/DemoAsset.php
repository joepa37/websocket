<?php
/**
 * @link https://plus.google.com/+joepa37/
 * @copyright Copyright (c) 2017 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace app\modules\websocket\assets;

use yii\web\AssetBundle;

class DemoAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/websocket/assets/source';
    public $css = [
        'css/style.css',
    ];
    public $js = [
		'js/qz-demo.js',
    ];
    public $depends = [
        WebSocketAsset::class,
	    'yii\bootstrap\BootstrapAsset',
    ];
}
