<?php
/**
 * @link https://plus.google.com/+joepa37/
 * @copyright Copyright (c) 2017 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace app\modules\websocket\assets;

use yii\web\AssetBundle;
use yii\web\View;

class WebSocketAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/websocket/assets/source';
    public $js = [
		'js/qz-tray.js',
		'js/dependencies/rsvp-3.1.0.min.js',
		'js/dependencies/sha-256.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

	/**
	 * Registers the CSS and JS files with the given view.
	 * @param \yii\web\View $view the view that the asset files are to be registered with.
	 */
	public function registerAssetFiles($view)
	{
		parent::registerAssetFiles($view);
		$js = <<<JS
webSocketBaseUrl = "$this->baseUrl";
JS;
		$view->registerJs($js, View::POS_HEAD);

		/// Authentication setup
		$security = <<<JS
qz.security.setCertificatePromise(function(resolve, reject) {
    $.ajax("/websocket/default/certificate").then(resolve, reject);
});
qz.security.setSignaturePromise(function(toSign) {
    return function(resolve, reject) {
        $.ajax("/websocket/default/signing?request=" + toSign).then(resolve, reject);
    };
});
JS;
		$view->registerJs($security);
	}
}
