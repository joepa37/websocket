<?php

namespace app\modules\websocket\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;

/**
 * Default controller for the `WebSocket` module
 */
class DefaultController extends Controller
{
    public $enableOnlyActions = ['demo'];

	/**
	 * Renders the demo view for the module
	 * @return string
	 */
    public function actionDemo()
    {
	    return $this->render('demo');
    }

	/**
	 * Renders the test view for the module
	 * @return string
	 */
	public function actionTest()
	{
		return $this->render('test');
	}

	/**
	 * Return the certificate (public key) to be used for singing method
	 * @return string
	 */
	public function actionCertificate()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$CERTIFICATE = dirname(__DIR__).'/certificate/override.crt';
		return file_get_contents($CERTIFICATE);
	}

	/**
	 * Return the generated signature from open ssl sign given request param
	 * to try the connection to the web socket
	 * @return string
	 */
	public function actionSigning()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$KEY = dirname(__DIR__).'/certificate/private-key.pem';
		$req = Yii::$app->request->get()['request'];
		$privateKey = openssl_get_privatekey(file_get_contents($KEY));
		$signature = null;
		openssl_sign($req, $signature, $privateKey);
		if ($signature) {
			return base64_encode($signature);
		}
		return '<h1>Ocurrió un error al intentar firmar la petición con la llave privada.</h1>';
	}
}