﻿# WEB SOCKET MODULE
Browser plugin for sending documents and raw commands to a printer or attached device.
QZ Tray Connector connects a web client to the QZ Tray software. Enables printing and device communication from javascript.

## Content

1. [Prerequisites](#prerequisites)
   * [Compatibility](#compatibility)
   * [Dependencies](#dependencies)
   * [Installers](#installers)
1. [Module Configuration & Usage](#module-configuration-usage)
   * [Module Configuration](#module-configuration)
   * [QZ Tray Demo](#qz-tray-demo)
   * [Printer Widget Requirement](#printer-widget-requirement)
   * [Basic Usage](#basic-usage)
      * [HTML Printing](#html-printing)
          * [RAW HTML](#raw-html)
          * [HTML File](#html-file)
          * [Orientation](#orientation)
          * [Page Width & Height](#page-width-height)
      * [PDF Printing](#pdf-printing)
          * [PDF File](#pdf-file)
          * [Base64 PDF](#base64-pdf)
      * [Image Printing](#image-printing)
          * [Image File](#image-file)
          * [Base64 Image](#base64-image)
   * [Advance Usage](#advance-usage)
      * [Chaining Requests](#chaining-requests)
      * [Page Size](#page-size)
      * [Margins](#margins)
      * [Image Interpolation](#image-interpolation)
      * [Density](#density)
      * [Unit](#unit)
      * [Color](#color)
      * [Copies](#copies)
      * [Custom Job Name](#custom-job-name)
      * [Disable Autoscale](#disable-autoscale)
      * [Rasterize](#rasterize)
1. [Configure Server](#configure-server)
   * [Generate OpenSSL Certificate](#generate-openssl-certificate)
1. [Configure Client](#configure-client)
   * [Install & configure QZ Tray](#install-configure-qz-tray)
   * [Where QZ Tray could be listening on](#where-qz-tray-could-be-listening-on)

### Prerequisites

Verify this requirements before start using QZ Tray web socket 

#### Compatibility

| :white_check_mark: QZ Tray 2.0.4 | :white_check_mark: Java 8 / OpenJDK 1.8 |
| -------------------------------- | --------------------------------------- |

#### Dependencies

| Library                                                           | Version |
| ----------------------------------------------------------------- | ------- |
| [RSVP](assets/source/js/dependencies/rsvp-3.1.0.min.js)           | 3.1.0   |
| [Sha256](assets/source/js/dependencies/sha-256.min.js)            |         |
| [JQuery](assets/source/js/additional/jquery-1.11.3.min.js)        | 1.11.3  |
| [QZ Tray Connector](assets/source/js/qz-tray.js)                  | 2.0.4   |

#### Installers

| Software                                                                       | Minimum Version |
| ------------------------------------------------------------------------------ |---------------- |
| [QZ Tray Software](installers/qz-tray-2.0.4.exe)                               | 2.0.4           |
| [OpenSSL](installers/openssl-0.9.8h-1-setup.exe)                               | 0.9.8h          |
| [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) | 1.8.0 U45       |

### Module Configuration & Usage

Once the requirements are covered you can start integrating the code into the environment

#### Module Configuration
 * Add to config/web Modules section

    ```php
    'modules'=>[
        'websocket' => [
           'class' => 'app\modules\websocket\WebSocketModule'
        ],
    ],
    ```
   
#### QZ Tray Demo
To view the QZ LWS Demo go to: [lws.dev/websocket/default/demo](http://lws.dev/printer/default/demo)

#### Printer Widget Requirement
To run the [Printer::widget](widgets/PrinterWidget.php) is required a `view` file where the [WebSocketAsset](assets/WebSocketAsset.php) will be registered

#### Basic Usage

 * Enable the debug console log with `'debug' => true`
 * Disable all the layout rendering with `'template' => ""`
 * Rewrite the template using `'template' => "{printButton}"` with the elements you want to render: `{alerts}` `{launchButton}` `{printButton}` `{statusHeading}` 
 * Rewrite the html render for any element in the template with `'printButton' => '<button id="print" href="#" onclick="printData();">Imprimir</button>'` but remember to use the id/onclick for the elements used in the widget to apply the JS functionality
 * The widget will select the default configured printer on the compouter, to use a specific printer use the `'printer' => 'Star'` variable. It will query for the printer, is not needed to specify the entire printer name
 * The `'data'` is required and must be set as a multidimensional array to benefit the multi printer trigger
 * Start printing after the connection is established with `'printOnInit' => true`
 * Run a trigger after or before an action using the following variables
    *  `'BEFORE_CONNECT' => ''`
    *  `'AFTER_CONNECT' => ''`
    *  `'BEFORE_SET_PRINTER' => ''`
    *  `'AFTER_SET_PRINTER' => ''`
    *  `'BEFORE_PRINT' => ''`
    *  `'AFTER_PRINT' => ''`
    * Example:

        ```php
        <?= Printer::widget([
            'BEFORE_CONNECT' => new \yii\web\JsExpression('console.log("Trigger before connect");'),
            'AFTER_CONNECT' => new \yii\web\JsExpression('console.log("Trigger after connect");'),
            'BEFORE_SET_PRINTER' => new \yii\web\JsExpression('console.log("Trigger before set printer");'),
            'AFTER_SET_PRINTER' => new \yii\web\JsExpression('console.log("Trigger after set printer");'),
            'BEFORE_PRINT' => new \yii\web\JsExpression('console.log("Trigger before print");'),
            'AFTER_PRINT' => new \yii\web\JsExpression('console.log("Trigger after print");'),
        ]); ?>
        ```
 
##### HTML Printing

 * HTML rendering is done using `'type' => 'html'` and `'format' => 'plain'` configuration

    ###### RAW HTML
    
     ```php
   <?php
       $html_test = <<<HTML
   <html>
       <h2 style="color: #F00;"> Printer Widget - HTML Printing</h2>
   </html>
   HTML;
   ?>
    <?= Printer::widget([
 	  'printer' => 'HP',
      'data' => [
          [
              'type' => 'html',
              'format' => 'plain',
              'data' => $html_test,
          ],
      ],
   ]);  ?>
    ```
    
    ###### HTML File

      ```php
   <?= Printer::widget([
      'data' => [
          [
              'type' => 'html',
              'format' => 'file',
              'data' => 'assets/html_sample.html' //The path of the file,
          ],
      ],
   ]);  ?>
      ```

 * The `config` array can be used to signify things including margins, orientation, size, printer language, encoding, etc.

    ###### Orientation

      ```php
   <?= Printer::widget([
      'data' => [
          [
              'type' => 'html',
              'format' => 'plain',
              'data' => $html_test,
          ],
      ],
      'config' => [
           'orientation' => 'landscape',
           'margin' => 2,
      ],
   ]);  ?>
      ```
      
 * The `data[options[ ]]` array can be used to signify things including x position, y position, xmlTag, page width, page height, etc.

    ###### Page Width & Height

      ```php
   <?= Printer::widget([
      'data' => [
          [
              'type' => 'html',
              'format' => 'plain',
              'data' => $html_test,
              'options' => [
           	        'pageWidth' => 8,
        	        'pageHeight' => 11.5
               ],
          ],
      ],
   ]);  ?>
      ```

##### PDF Printing

 * Print PDF files directly to a printer using Apache PDFBOX

    ###### PDF File

      ```php
   <?= Printer::widget([
      'data' => [
          [
              'type' => 'pdf',
              'data' => 'assets/pdf_sample.pdf' //The path of the file,
          ],
      ],
   ]);  ?>
      ```

    ###### Base64 PDF

      ```php
   <?= Printer::widget([
      'data' => [
          [
              'type' => 'pdf',
              'format' => 'base64',
              'data' => 'Ck4KcTYwOQpRMjAzLDI2CkI1LDI2LDAsMUEsMyw3LDE1MixCLCIxMjM0IgpBMzEwLDI2LDAsMywx...',
          ],
      ],
   ]);  ?>
      ```

##### Image Printing

 * Print Image directly

    ###### Image File

      ```php
   <?= Printer::widget([
      'data' => [
          [
              'type' => 'image',
              'data' => 'assets/img/image_sample.png' //The path of the file,
          ],
      ],
   ]);  ?>
      ```

    ###### Base64 Image

      ```php
   <?= Printer::widget([
      'data' => [
          [
              'type' => 'image',
              'format' => 'base64',
              'data' => 'AAAA...==',
          ],
      ],
   ]);  ?>
      ```
      
#### Advance Usage

More considerations using by cases

##### Chaining Requests
   Example using the [`WebSocketAsset`](assets/WebSocketAsset.php) chaining requests with [Promise driven asynchronous](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Promise)
   
   * List all the printers attached to the system

   ```php
<h3 id="messaage"></h3>

<?php
    WebSocketAsset::register($this);

	$js = <<<JS
qz.websocket.connect().then(function() {
    qz.printers.find().then(function(data) {
        var list = '';
        for(var i = 0; i < data.length; i++) {
            list += "&nbsp; " + data[i] + "<br/>";
        }
        var message = $("#messaage");
        message.html("<strong>Available printers:</strong><br/>" + list);
    }).catch(function(e) { console.error(e); });
});
JS;
	
	$this->registerJs($js);
?>
   ```
   
   * Silent Printing
   
   ```php
<?php
    WebSocketAsset::register($this);

    $js = <<<JS
qz.websocket.connect().then(function() {
    return qz.printers.find("HP")               // Pass the printer name into the next Promise
}).then(function(printer) {
var config = qz.configs.create(
    	printer,  								// Printer returned on the last Promise
   		{ jobName: "Esta es una prueba" }	   	// Config to apply to the current print
    );
var data = [{                                   //PDF file
    type: 'pdf', 
    data: webSocketBaseUrl + '/samples/pdf_sample.pdf' 
}];
    return qz.print(config, data);
}).catch(function(e) { console.error(e); });
JS;
    
  $this->registerJs($js);
?>
   ```

##### Page Size
   A page size can be set using the printer configuration parameter `config[size[]]`. Both standard and metric sizes are supported.  Warning, providing a metric page size will assume the `config[density]` is in a metric format as well 
   * `size (array)`
       * `width (number)`
       * `height (number)`

##### Margins
   A `config[margins[]]` parameter can be provided to change the default page margins around the content. 
   * `margins (array)`
       * `top (number)`
       * `right (number)`
       * `bottom (number)`
       * `left (number)`

##### Image Interpolation
   A `config[interpolation]` parameter can be provided to change the pixel blending technique used when scaling an image.
   * `interpolation (string)` valid values: `[bicubic | bilinear | nearest-neighbor]`

##### Density
   Not providing a density will cause printing to use the default value for the printer. A `config[density]` parameter can be provided to change the DPI, dots/mm or dots/cm respectively.
   * `deinsity (number|array depending on unit value)`
   
##### Unit
   A `config[unit]` parameter can be provided to apply to paper size, margins, and density.
   * `unit (string)` valid values: `[in | cm | mm]`
   
##### Color
   A `config[colorType]` parameter can be provided.
   * `colorType (string)` valid values: `[color | grayscale | blackwhite]`

##### Copies
   A `config[copies]` parameter can be provided to send the specified raw commands multiple times.
   * `copies (number)`

##### Custom Job Name
   A `config[jobName]` parameter can be provided to change the name listed in the print queue.
   * `jobName (string)`

##### Disable Autoscale
   By default, content is automatically scaled to the destination page size.  This is done as a courtesy to take out the guesswork in fitting the content to the media.  A `config[scaleContent]` parameter can be forced to prevent auto-scaling.
   * `scaleContent (boolean)`

##### Rasterize
   In some cases, rather than rasterizing print output, the vector version is preferred.  This can have both quality as well as performance benefits during the printing process.  A `config[rasterize]` parameter can be forced to prevent software rasterization prior to printing.
   * `rasterize (boolean)`

### Configure Server

##### Generate OpenSSL Certificate
OpenSSL is used to create the self signed certificate.
 * [Install OpenSSL](installers/openssl-0.9.8h-1-setup.exe) software
 * Add the `C:\Program Files (x86)\GnuWin32\bin` path to the `System Environment Variables`
 * Run the following commands
    
    * Move to [certificate](certificate) folder
    
        ```cmd
        cd /modules/printer/certificate
        ```
    
    * Rewrite `OPENSSL_CONF` Path to prevent `Unable to load config info from /usr/local/ssl/openssl.cnf` error.
        
        ```cmd
        set OPENSSL_CONF=C:\Program Files (x86)\GnuWin32\share\openssl.cnf
        ```
        
    * Generate the certificate for 365 days
        
        ```cmd
        openssl req -new -newkey rsa:2048 -sha256 -days 365 -nodes -x509 -keyout private-key.pem -out override.crt
        ```

    The following information will be required in making the certificate:
    * Country Name (2 letter code): `HN`
    * State or Province Name (full name): `Atlántida`
    * Locality Name (eg, city): `La Ceiba`
    * Organization Name (eg, company): `CETI`
    * Organizational Unit Name (eg, section): `IT`
    * Common Name (eg, YOUR name): `lws.dev` or `*.lws.dev` `This entry is important, this should be the domain name in wildcard format or including subdomain query.`
    * Email Address: `info@ceti.systems`
    
    Once executed the commands two files will be generated:
    * [Certificate (Public Key)](certificate/override.crt) `override.crt`. This will be used on the function `actionCertificate()` in [`<DefaultController>`](controllers/DefaultController.php). This certificate need to be provided to `QZ Try Software` on the next step.
    * [RSA (Private Key)](certificate/private-key.pem) `private-key.pem`. This will be used on the authentication function `actionSigning()` in [`<DefaultController>`](controllers/DefaultController.php) to signature the `JavaScript` requests with `openssl_get_privatekey` php function.
    
### Configure Client
QZ Tray Software is used as a cross-browser, cross-platform plugin to enable communication to the devices from JavaScript.

##### Install & configure QZ Tray
 * [Install Java 8 or higher](http://www.oracle.com/technetwork/java/javase/downloads/index.html) from Oracle's website
 * [Install QZ Tray Software](installers/qz-tray-2.0.4.exe)
 * Copy the [Certificate](certificate/override.crt) to `C:\Program Files\QZ Tray` where QZ tray is already installed
 * Run the `Notepad` as `Administrator` and open the `qz-tray.properties` file.
 * Add the follow line, save and close the file
 
    ```bash
    authcert.override=override.crt
    ```
 
 * Close and run again the `QZ Tray Software`
 * Open [QZ LWS Demo](http://lws.dev/printer/default/demo)
 * Will get a popup that you can check `Remember this decision` and make it go away forever
 
    ![Action Required](assets/source/img/Action%20Required.PNG)
 
 * In `Certificate Information` will appear the Signature Promise used for the connection
 
    ![Certificate](assets/source/img/Certificate.PNG)
 
##### Where QZ Tray could be listening on
| Protocol       | Ports                  | Web Socket |  Hosts                            |
| -------------- | ---------------------- | ---------- | --------------------------------- | 
| HTTPS (Secure) | 8181, 8282, 8383, 8484 | `"wss://"` | `"localhost"` `"localhost.qz.io"` |
| HTTP (Insecure)| 8182, 8283, 8384, 8485 | `"ws://"`  | `"localhost"` `"localhost.qz.io"` |